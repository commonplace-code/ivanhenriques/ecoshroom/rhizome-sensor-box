# Rizoom box sensors

## Sensors:

| Component | Type | Product code | I2C Address |		
| --------- | ---- | ------------ | ----------- |
Electrical ADC | ADS1115 | ADA-1085 | 0x48 - 0x4B
Light | Adafruit TSL2591 | ADA-1980 | 0x29 - 0x28
Soil humidity | Grove - Moisture Sensor | SS-101020008 | -
CO2	| Grove CO2 ACD41 | SS-101020952 | 0x62
Temperature / Humidity | Grove Temp & Humidity DHT20 | SS-101020932	| 0x38
Adafruit feather M0 controller | Controller	| ADA-2796 | -
Grove I2C Hub | I2C hub |	SS-103020006 | -
Cabling	| Cabling | SS-11090040 | -

## Data format

CSV - tab seperated

### Sensirion ACD41 | CO2, Temperature, Humidity

CO2 | Ambient temperature | Relative humidity

### ADS1115 | 4x Electrical signals

Exp 1: ADC0: (**Red or Green**) | ADC1 (**Blue**) | ADC2 (**Yellow**) | ADC3 (**White**, reference)

Exp 2: ADC0: **White**, ADC1: **Yellow**, ADC2: **Green**, ADC3: (**Blue** reference)

### TSL2591 | Ambient light

IR | Full spectrum | Visible | Calculated Lux

### DHT20 | Humidity, Temperature

Ambient temperature | Relative humidity

### Soil sensor | soil resistance

Analog reading 0-1024

### Plant signals

ADC0: Red / Green is in plant

ADC1: Blue: plant/mico

ADC2: Yellow: mico

### CSV Table format

| Sequence | Timestamp (s) | ADC1 (V) | ADC2 (V) | ADC3 (V) | Soil (0-1023) | IR Luminosity (J/s) | Full Spectrum Luminosity (J/s) | Visible (Full - IR) Luminosity (J/s) | Calculated Lux | CO2 (PPM) | Ambient Temperature (°C) | Relative Humidity (%) |
| --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |

![Sensor Overview](doc/sensor-overview.png)

# LOG Protocol

The sensor boxes will log the data to a CSV formatted file. There is no timekeeping (unfortunately no internal backup battery for the clock). So if the power is removed the clock resets to 0. 

A new log file is created with incremented number eacht time power is applied. To keep the correct log associated with the right experiment it is important to follow below protocol:

**When starting an experiment the following procedure should be followed:**

- Remove previous logs from SD card
- Apply power and note the date and time
- When changing a parameter, note the date and time
- When experiment is done:
    - remove the power
    - note date and time
    - note experiment duration
    - copy logs from SD-card to computer
    - remove logs from SD-card
    - note the log name in the experiment notes for later association
    
The log file names should be of the form `[logdate]_es[boxnumber]_on[ondate]_off[offdate].csv`
The date formats are of the form:

- logdate = `%d%m%y`
- ondate, offdate = `%H%M%d%m%y`

E.g.: `070424_es02_on1733040424_off1716070424.CSV`

# Boxes experiment A

- BOX1: es05 and es07 (no mycorrhizae)
- BOX2: es04 and es06 (Rhizopagus irregularis)
- BOX3: es02 and es03 (Funneliformis mosseae)

- ADC0: Red / Green is in plant*
- ADC1: Blue: plant/mico
- ADC2: Yellow: mico