/**
 * @file main.cpp
 * @author Commonplace Development (simon@commonplace.tech)
 * @brief
 * @version 0.1
 * @date 2024-02-04
 *
 * @copyright Copyright (c) 2024 Commonplace Development
 *
 */

#include <Adafruit_ADS1X15.h>
#include <Adafruit_seesaw.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_TSL2591_cp.h>
#include <Adafruit_AHTX0_cp.h>
#include <SensirionI2CScd4x.h>

#include <SPI.h>
#include <SD.h>

#define SENSOR_ADC_ENABLE       true
#define SENSOR_SOIL_ENABLE      true
#define SENSOR_LIGHT_ENABLE     true
#define SENSOR_HUMIDITY_ENABLE  true
#define SENSOR_CO2_ENABLE       true

#define ADC_SAMPLEREATE         (50u)
#define ADC_SAMPLE_PERIOD_MS    ((unsigned long)((1.f/(ADC_SAMPLEREATE))*1000u))
#define STORE_PERIOD_MS         (1000u)
#define FLUSH_PERIOD_MS         (60u * 1000u) // force flush every 1 minute
#define FLUSH_COUNTER           ((int)(FLUSH_PERIOD_MS / STORE_PERIOD_MS))

#define LED_PIN                 8

const char header[] = "Sequence\tDelta T (s)\tADC0 (V)\tADC1 (V)\tADC2 (V)\tMoisture\tIR (Lum)\tFull spectrum (Lum)\tVisible (Full-IR)\tCalculated Lux\tCO2 (ppm)\tTemperature (°C)\tRel. Humidity (%)\n";

typedef struct SDCard
{
  SDClass *sd;
  File log;
  int CS;
  bool present;
  String buffer;
} SDCard_t;

typedef struct sensor_adc
{
  Adafruit_ADS1115 sensor;
  float values[3];
  float tmp[3];
  int samples;
  bool enabled;
} sensor_adc_t;

typedef struct sensor_soil
{
  Adafruit_seesaw sensor;
  uint16_t value;
  bool enabled;
} sensor_soil_t;

typedef struct sensor_light
{
  Adafruit_TSL2591 sensor;
  uint16_t ir;
  uint16_t full;
  uint16_t visible;
  float lux_calculated;
  bool enabled;
} sensor_light_t;

typedef struct sensor_humidity
{
  Adafruit_AHTX0 sensor;
  float temperature;
  float rel_humidity;
  bool enabled;
} sensor_humidity_t;

typedef struct sensor_co2
{
  SensirionI2CScd4x sensor;
  uint16_t co2;
  float temperature;
  float humidity;
  bool enabled;
} sensor_co2_t;

typedef struct sensors
{
  sensor_adc_t adc;
  sensor_soil_t soil;
  sensor_light_t light;
  sensor_humidity_t humidity;
  sensor_co2_t co2;
  unsigned long start_t;
  unsigned long sequence;
} sensors_t;


static sensors_t sensors = {
  .adc = {
    .values = {0.0, 0.0, 0.0},
    .tmp = {0.0, 0.0, 0.0},
    .samples = 0u,
    .enabled = SENSOR_ADC_ENABLE
  },
  .soil = {
    .value = 0,
    .enabled = SENSOR_SOIL_ENABLE
  },
  .light = {
    .sensor = Adafruit_TSL2591(2591),
    .ir = 0,
    .full = 0,
    .visible = 0,
    .lux_calculated = 0.0,
    .enabled = SENSOR_LIGHT_ENABLE
  },
  .humidity = {
    .temperature = 0.0,
    .rel_humidity = 0.0,
    .enabled = SENSOR_HUMIDITY_ENABLE
  },
  .co2 = {
    .co2 = 0,
    .temperature = 0.0,
    .humidity = 0.0,
    .enabled = SENSOR_CO2_ENABLE
  },
  .start_t = 0u,
  .sequence = 0u,
};

static SDCard_t sdcard = {
  .sd = &SD,
  .CS = 4,
  .present = false
};

String data_buffer;
String serial_buffer;

typedef struct led
{
  unsigned long duration;
  unsigned long started;
} led_t;

static led_t led = {
  .duration = 0u,
  .started = 0u
};

void blinkLed(led_t *led, unsigned long duration)
{
  if (led->started > 0)
  {
    return;
  }

  led->duration = duration;
  led->started = millis();

  digitalWrite(LED_PIN, HIGH);
}

void ledWorker(led_t *led)
{
  if (led->started == 0)
  {
    return;
  }

  if (millis() - led->started >= led->duration)
  {
    digitalWrite(LED_PIN, LOW);
    led->started = 0;
    led->duration = 0;
  }
}

void writeBufferToFile(SDCard_t *sdcard)
{
  if (data_buffer.length() > 0)
  {
    unsigned int chunkSize = sdcard->log.availableForWrite();

    if (chunkSize && data_buffer.length() >= chunkSize)
    {
      unsigned int writeBytes = (data_buffer.length() > chunkSize) ? chunkSize : data_buffer.length();
      size_t bytesWritten = sdcard->log.write(data_buffer.c_str(), writeBytes);
      data_buffer.remove(0, bytesWritten);
      //Serial.println("Wrote to file: " + String(bytesWritten) + " bytes.");
      blinkLed(&led, 500);
    }
  }
}

bool initSd (SDCard_t *sdcard)
{
  if (!sdcard->sd->begin(sdcard->CS))
  {
    Serial.println("SD Card failed, or not present");
    return false;
  }

  //Serial.println("SD Card initialized.");
  sdcard->present = true;
  return true;
}

bool openNextAvailableFile(SDCard_t *sdcard)
{
  for (int i = 0; i < 9999; i++)
  {
    char filename[16] = {0};
    snprintf(filename,sizeof(filename)-1, "log%04d.csv", i);
    if (sdcard->sd->exists(filename))
    {
      continue;
    }
    sdcard->log = sdcard->sd->open(filename, FILE_WRITE);
    break;
  }

  return sdcard->log;
}

void printSensorInfo(sensors_t *sensors)
{
  data_buffer += "Sensor info:\n\n";

  if (sensors->adc.enabled)
  {
    data_buffer += "ADC:\n";
    data_buffer += "  Enabled: " + String(sensors->adc.enabled) + "\n";
    data_buffer += "  Sample rate: " + String(ADC_SAMPLEREATE) + "Hz\n";
    data_buffer += "  Channels:\n";
    data_buffer += "    ADC0 (Red or Green):  Differential 0-3\n";
    data_buffer += "    ADC1 (Blue): Differential 1-3\n";
    data_buffer += "    ADC2 (Yellow):  Differential 2-3\n";
    data_buffer += "    ADC3 (White):    Common\n";
    data_buffer += "  Gain: 8x, Range: +/- 0.512V, Resolution: 0.015625 mV\n";
    data_buffer += "  Averaging: 50 samples\n";
  }

  if (sensors->soil.enabled)
  {
    data_buffer += "Soil:\n";
    data_buffer += "  Enabled: " + String(sensors->soil.enabled) + "\n";
    data_buffer += "  Range: 200: dry, 2000: wet\n";
  }

  if (sensors->light.enabled)
  {
    data_buffer += "Light:\n";
    data_buffer += "  Enabled: " + String(sensors->soil.enabled) + "\n";
  }

  if (sensors->co2.enabled)
  {
    data_buffer += "CO2 & Humidity & Temperature:\n";
    data_buffer += "  Enabled: " + String(sensors->soil.enabled) + "\n";
  }
  else
  {
    data_buffer += "Humidity & Temperature:\n";
    data_buffer += "  Enabled: " + String(sensors->soil.enabled) + "\n";
  }

  Serial.println(data_buffer);
}

bool initADC(sensor_adc_t *sensor)
{
  if (!sensor->sensor.begin())
  {
    Serial.println("Failed to initialize ADS1115.");
    sensor->enabled = false;
    return false;
  }

  sensor->sensor.setGain(GAIN_EIGHT);
  sensor->sensor.setDataRate(RATE_ADS1115_250SPS);
  return true;
}

void sampleADC(sensor_adc_t *sensor)
{
  sensor->tmp[0] += sensor->sensor.computeVolts(sensor->sensor.readADC_Differential_0_3());
  sensor->tmp[1] += sensor->sensor.computeVolts(sensor->sensor.readADC_Differential_1_3());
  sensor->tmp[2] += sensor->sensor.computeVolts(sensor->sensor.readADC_Differential_2_3());
  sensor->samples++;
}

void readADC(sensor_adc_t *sensor)
{
  sensor->values[0] = sensor->tmp[0] / sensor->samples;
  sensor->values[1] = sensor->tmp[1] / sensor->samples;
  sensor->values[2] = sensor->tmp[2] / sensor->samples;
  sensor->tmp[0] = sensor->tmp[1] = sensor->tmp[2] = 0.f;
  sensor->samples = 0;
}

bool initSoilSensor(sensor_soil_t *sensor)
{
  /*
  if (!sensor->sensor.begin(0x36))
  {
    Serial.println("Failed to initialize soil sensor.");
    sensor->enabled = false;
    return false;
  }
  */
  analogReadResolution(10);

  return true;
}

void readSoilSensor(sensor_soil_t *sensor)
{
  //sensor->value = sensor->sensor.touchRead(0);
  sensor->value = analogRead(A0);
}

bool initLightSensor(sensor_light_t *sensor)
{
  if (!sensor->sensor.begin())
  {
    Serial.println("Failed to initialize light sensor.");
    sensor->enabled = false;
    return false;
  }

  sensor->sensor.setGain(TSL2591_GAIN_MED);
  sensor->sensor.setTiming(TSL2591_INTEGRATIONTIME_100MS);

  return true;
}

void readLightSensor(sensor_light_t *sensor)
{
  uint32_t lum = sensor->sensor.getFullLuminosityNonBlock();
  sensor->ir = lum >> 16;
  sensor->full = lum & 0xFFFF;
  sensor->visible = sensor->full - sensor->ir;
  sensor->lux_calculated = sensor->sensor.calculateLux(sensor->full, sensor->ir);
}

bool initHumiditySensor(sensor_humidity_t *sensor)
{
  if (!sensor->sensor.begin())
  {
    Serial.println("Failed to initialize humidity sensor.");
    sensor->enabled = false;
    return false;
  }

  return true;
}

void readHumiditySensor(sensor_humidity_t *sensor)
{
  sensors_event_t humidity, temp;
  if (sensor->sensor.getEventNonBlock(&humidity, &temp))
  {
    sensor->temperature = temp.temperature;
    sensor->rel_humidity = humidity.relative_humidity;
  }
}

bool initCo2Sensor(sensor_co2_t *sensor)
{
  uint16_t ret;
  char errorstr[256];

  Wire.begin();
  sensor->sensor.begin(Wire);

  ret = sensor->sensor.stopPeriodicMeasurement();
  if (ret)
  {
    Serial.print("Failed to initialize CO2 sensor: ");
    errorToString(ret, errorstr, 256);
    Serial.println(errorstr);
    sensor->enabled = false;
    return false;
  }

  ret = sensor->sensor.startPeriodicMeasurement();
  if (ret)
  {
    Serial.print("Failed to start periodic measurement: ");
    errorToString(ret, errorstr, 256);
    Serial.println(errorstr);
    sensor->enabled = false;
    return false;
  }

  return true;
}

void readCo2Sensor(sensor_co2_t *sensor)
{
  uint16_t ret;
  bool hasData = false;

  float temperature = 0.0f;
  float humidity = 0.0f;
  uint16_t co2 = 0;


  ret = sensor->sensor.getDataReadyFlag(hasData);
  if (ret)
  {
    sensor->co2 = 0;
    sensor->temperature = 0.0;
    sensor->humidity = 0.0;
    return;
  }

  if (!hasData)
  {
    return;
  }

  ret = sensor->sensor.readMeasurement(co2, temperature, humidity);
  if (ret) {
    sensor->co2 = 0;
    sensor->temperature = 0.0;
    sensor->humidity = 0.0;
    return;
  }

  if (co2 > 0)
  {
    sensor->co2 = co2;
    sensor->temperature = temperature;
    sensor->humidity = humidity;
  }

  return;
}


void setup(void)
{
  Serial.begin(115200);

  pinMode(LED_PIN, OUTPUT);

  delay(1000);

  if (sensors.adc.enabled)
  {
    initADC(&sensors.adc);
  }

  if (sensors.soil.enabled)
  {
    initSoilSensor(&sensors.soil);
  }

  if (sensors.light.enabled)
  {
    initLightSensor(&sensors.light);
  }

  if (sensors.co2.enabled)
  {
    initCo2Sensor(&sensors.co2);
  }

  if (sensors.humidity.enabled)
  {
    initHumiditySensor(&sensors.humidity);
  }

  initSd(&sdcard);
  if (sdcard.present)
  {
    openNextAvailableFile(&sdcard);
    if (!sdcard.log)
    {
      Serial.println("Could not open file for writing.");
    }
    else
    {
      Serial.println("Logging to file: " + String(sdcard.log.name()));
    }
  }

  data_buffer.reserve(1024);
  serial_buffer.reserve(1024);

  printSensorInfo(&sensors);

  data_buffer += "\n\n";
  data_buffer += header;
  writeBufferToFile(&sdcard);

  digitalWrite(LED_PIN, HIGH);
  delay(1000);
  digitalWrite(LED_PIN, LOW);
}

void loop(void)
{
  static unsigned long adc_lastSampleTime = millis();
  static unsigned long store_lastTime = millis();
  static int flush_counter = 0;

  unsigned long now = millis();

  if (now - adc_lastSampleTime >= ADC_SAMPLE_PERIOD_MS)
  {
    if (sensors.adc.enabled)
    {
      sensor_adc_t *sensor = &sensors.adc;
      sampleADC(sensor);
    }

    adc_lastSampleTime = now;
  }

  if (now - store_lastTime >= STORE_PERIOD_MS)
  {
    serial_buffer = "";

    if (sensors.sequence == 0)
    {
      sensors.start_t = now;
    }

    serial_buffer += String(sensors.sequence) + "\t";
    serial_buffer += String((now - sensors.start_t) / 1000.f, 1) + "\t";

    if (sensors.adc.enabled)
    {
      sensor_adc_t *sensor = &sensors.adc;
      //Serial.print(sensor->samples); Serial.print("\t");
      readADC(sensor);
      for (int i = 0; i < 3; i++)
      {
        serial_buffer += String(sensor->values[i], 4) + "\t";
      }
    }
    else
    {
      serial_buffer += "0\t0\t0\t";
    }

    if (sensors.soil.enabled)
    {
      sensor_soil_t *sensor = &sensors.soil;
      readSoilSensor(sensor);
      serial_buffer += String(sensor->value) + "\t";
    }
    else
    {
      serial_buffer += "0\t";
    }

    if (sensors.light.enabled)
    {
      sensor_light_t *sensor = &sensors.light;
      readLightSensor(sensor);
      serial_buffer += String(sensor->ir) + "\t" +
                       String(sensor->full) + "\t" +
                       String(sensor->visible) + "\t" +
                       String(sensor->lux_calculated, 3) + "\t";
    }
    else
    {
      serial_buffer += "0\t0\t0\t0\t";
    }

    if (sensors.co2.enabled)
    {
      sensor_co2_t *sensor = &sensors.co2;
      sensor->sensor.readMeasurement(sensor->co2, sensor->temperature, sensor->humidity);
      serial_buffer += String(sensor->co2) + "\t" +
                       String(sensor->temperature) + "\t" +
                       String(sensor->humidity) + "\t";
    }
    else if (sensors.humidity.enabled)
    {
      sensor_humidity_t *sensor = &sensors.humidity;
      readHumiditySensor(sensor);
      serial_buffer += "0\t" +
                       String(sensor->temperature, 2) + "\t" +
                       String(sensor->rel_humidity, 2) + "\t";
    }
    else
    {
      serial_buffer += "0\t0\t0\t";
    }

    serial_buffer += "\n";
    Serial.print(serial_buffer);

    data_buffer += serial_buffer;

    store_lastTime = now;
    sensors.sequence++;

    /* force flush */
    if (++flush_counter >= FLUSH_COUNTER)
    {
      sdcard.log.flush();
      flush_counter = 0;
    }
  }

  writeBufferToFile(&sdcard);
  ledWorker(&led);
}